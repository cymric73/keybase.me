# README #

Keybase.me - see https://keybase.me/about

### How do I get set up? ###

* Codebase is just HTML, JavaScript and CSS.  
* Uncomment //DEV line in kb.me.js to specify a user in developer mode.
* When hosted at a proper URL, kb.me.js file reads the hostname portion of the
  URL as the user parameter.  Some nginx config helps with proxy mapping & user wildcard.

### nginx

* See nginx.conf.kb.me.sample
* Omits https stuff but you get the idea.

### Who do I talk to? ###

* Repo owner = pmarchant.keybase.me
